#include "putcharJP.h"
#include <fstream>
#include "opencv2\opencv.hpp"
#include "boost\filesystem.hpp"
#include "boost\algorithm\string.hpp"
#include "boost\lexical_cast.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional.hpp>

using namespace boost::property_tree;
//#include "boost\filesystem\path.hpp"

namespace fs = boost::filesystem;
namespace alg = boost::algorithm;

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	putcharJP put("ＭＳゴシック");
	Mat mat = Mat::zeros(100, 200, CV_8UC3);
	put.putText(mat, "あいうえおかきくけこ", Point2f(0, 0), Scalar(255, 0, 0));
	imshow("test", mat);
	waitKey();

	// 引数の型変換（char* to string)
	vector<string> arg;
	for(int i = 1; i < argc; i++)
		arg.push_back(argv[i]);

	// 引数
	vector<string> chars;
	vector<string> fonts;
	string directory = "./";
	int font_size = 80;
	int img_width = 200;
	int img_height = 200;
	Scalar bg_color = CV_RGB(255,255,255);
	Scalar char_color = CV_RGB(0,0,0);
	bool inv = false;

	// 引数を解析
	for(int i = 0; i < (int)arg.size(); i+=2)
	{
		if(arg[i] == "-help")
		{
			cout << endl;
			cout << "※フォントと文字は，引数を複数指定できる追加方式．" << endl;
			cout << "「-font ＭＳゴシック -font ＭＳ明朝」のように，複数の指定が可能．" << endl;
			cout << "また，文字は画像の中心に置かれる．" << endl;
			cout << endl;
			cout << "-font	フォント（複数指定可能）" << endl;
			cout << "-fsize	フォントサイズ(default=80)" << endl;
			cout << "-char	作成する文字（複数指定可能）" << endl;
			cout << "-ctxt	作成する文字のリスト（複数指定可能．ASIIコードで，１行に１文字とする）" << endl;
			cout << "-ftxt	作成するフォントのリスト（複数指定可能．ACSIIコードで，１行に１フォントとする）" << endl;
			cout << "-output	出力先（ただし出力先には，更にフォントごとにディレクトリが作成される．default=カレントディレクトリ）" << endl;
			cout << "-width	画像の横の長さ(default=200)" << endl;
			cout << "-height	画像の縦の長さ(default=200)" << endl;
			cout << "-bcolor	背景色（「-bcolor 0 0 255」のように，引数はRGBとする．default=白）" << endl;
			cout << "-ccolor	文字色（「-bcolor 0 0 255」のように，引数はRGBとする．default=黒）" << endl;
			cout << "-inv	背景色と文字色を反転（フォントディレクトリ名は，「フォント_inv」となる）" << endl;
			cout << endl;
			cout << "例：「あ」とctxt.txtにある文字を「ＭＳゴシック」，フォントサイズ100，背景色と文字色を反転させて出力したい場合:" << endl;
			cout << "-char あ -ctxt ctxt.txt -font ＭＳゴシック -fsize 100 -inv" << endl;
			cout << endl;
			exit(1);
		}
		else if(arg[i] == "-font")
			fonts.push_back(arg[i+1]);
		else if(arg[i] == "-fsize")
			font_size = atoi(arg[i+1].c_str());
		else if(arg[i] == "-char")
			chars.push_back(arg[i+1]);
		else if(arg[i] == "-ctxt")
		{
			fstream ifs(arg[i+1]);
			char tmp[100];
			while(!ifs.eof())
			{
				ifs.getline(tmp, 100-1);
				chars.push_back(tmp);
			}
			ifs.close();
		}
		else if(arg[i] == "-ftxt")
		{
			fstream ifs(arg[i+1]);
			char tmp[100];
			while(!ifs.eof())
			{
				ifs.getline(tmp, 100-1);
				fonts.push_back(tmp);
			}
			ifs.close();
		}
		else if(arg[i] == "-output")
			directory = arg[i+1];
		else if(arg[i] == "-width")
			img_width = atoi(arg[i+1].c_str());
		else if(arg[i] == "-height")
			img_height = atoi(arg[i+1].c_str());
		else if(arg[i] == "-bcolor")
		{
			int r,g,b;
			r = boost::lexical_cast<int>(arg[i+1]);
			g = boost::lexical_cast<int>(arg[i+2]);
			b = boost::lexical_cast<int>(arg[i+3]);
			bg_color = CV_RGB(r,g,b);
			i+=2;
		}
		else if(arg[i] == "-ccolor")
		{
			int r,g,b;
			r = boost::lexical_cast<int>(arg[i+1]);
			g = boost::lexical_cast<int>(arg[i+2]);
			b = boost::lexical_cast<int>(arg[i+3]);
			char_color = CV_RGB(r,g,b);
			i+=2;
		}
		else if(arg[i] == "-inv")
		{
			inv = true;
			i--;
		}
		else
		{
			cerr << "正しい引数を入力してください．" << endl;
			exit(1);
		}
	}

	if(chars.empty())
	{
		cerr << "対象となる文字がありません．" << endl;
		exit(2);
	}
	if(fonts.empty())
	{
		cerr << "フォントが選択されていません．" << endl;
		exit(3);
	}

	if(!fs::exists(directory))
		fs::create_directories(directory);

	for(int f = 0; f < (int)fonts.size(); f++)
	{
		cout << "FONT = " << fonts[f] << endl;
		string font_directory, tmp;
		tmp = fonts[f];
		alg::replace_all(tmp, " ", "_");
		alg::replace_all(tmp, "　", "_");
		font_directory = directory + "/" + tmp;
		if(inv)
			font_directory += "_inv";
		if(!fs::exists(font_directory))
			fs::create_directory(font_directory);

		putcharJP jp(fonts[f], font_size);
		for(int i = 0; i < (int)chars.size(); i++)
		{
			// アルファベットの大文字と小文字を区別する
			string chr = chars[i];
			if(chr[0] == '_')
				chr.erase(chr.begin());

			Mat img;
			if(!inv)
				jp.putFittingChar(&img, chr, img_width, img_height, char_color, bg_color);
			else
				jp.putFittingChar(&img, chr, img_width, img_height, bg_color, char_color);
			string output_path;
			output_path = font_directory + "/" + chars[i] + ".png";
			imwrite(output_path, img);
		}

		ptree pt;
		pt.put("Data.font", fonts[f]);
		pt.put("Data.font_size", font_size);
		pt.put("Data.width", img_width);
		pt.put("Data.height", img_height);
		write_ini(font_directory += "/config.ini", pt);
	}
	return 0;
}