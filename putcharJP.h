#pragma once

#ifdef _OPENMP
#include <omp.h> 
#endif

#define GRAY_LEVEL(_X_) ( 255l * (_X_) / ( iLevel -1 ))

#include <Windows.h>
#include <string>
#include "opencv2\opencv.hpp"

class putcharJP
{
public:
	putcharJP(const std::string &fontName ="メイリオ", int _fontSize = 32, int _weight = 0);
	~putcharJP(void);

	// 1文字描画
	void putChar( cv::Mat &mat, char *c, cv::Point2i pos, cv::Scalar sc, int _gray = GGO_GRAY8_BITMAP );

	void putcharJP::putText( cv::Mat &mat, TCHAR *c, cv::Point2i pos, cv::Scalar sc, int _gray = GGO_GRAY8_BITMAP );

	// 1文字描画
	void putFittingChar(cv::Mat *mat, std::string c, int margin = 0, cv::Scalar sc = CV_RGB(0,0,0), cv::Scalar bg_color = CV_RGB(255,255,255), int _gray = GGO_GRAY8_BITMAP);
	void putFittingChar(cv::Mat *mat, char *c, int margin = 0, cv::Scalar sc = CV_RGB(0,0,0), cv::Scalar bg_color = CV_RGB(255,255,255), int _gray = GGO_GRAY8_BITMAP );
	
	void putFittingChar(cv::Mat *mat, std::string c, int width, int height, cv::Scalar sc = CV_RGB(0,0,0), cv::Scalar bg_color = CV_RGB(255,255,255), int _gray = GGO_GRAY8_BITMAP );

private:
	LOGFONT		lFont;		// フォント取得用データ
	HFONT		hFont;		// フォントハンドル
	HFONT		oldFont;	// 元々のフォントのハンドル
	HDC			hdc;		// フォント用のデバイスコンテキスト
	TEXTMETRIC	tm;			// テキストメトリック
	int			iCursor_x;	// 表示用 X 座標
	int			iCursor_y;	// 表示用 X 座標

	
};

