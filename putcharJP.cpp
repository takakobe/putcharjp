#include "putcharJP.h"

using namespace std;
using namespace cv;

putcharJP::putcharJP( const string &_fontName, int _fontSize, int _weight)
: iCursor_x(0), iCursor_y(0){
		//
		lFont.lfHeight = _fontSize;
		lFont.lfWidth = 0;
		lFont.lfEscapement = 0;
		lFont.lfOrientation = 0;
		lFont.lfWeight = _weight;
		lFont.lfItalic = 0;
		lFont.lfUnderline = 0;
		lFont.lfStrikeOut = 0;
		lFont.lfCharSet = DEFAULT_CHARSET;				// SHIFTJIS_CHARSET;
		lFont.lfOutPrecision = OUT_DEFAULT_PRECIS;		// OUT_TT_ONLY_PRECIS; // ??
		lFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
		lFont.lfQuality = ANTIALIASED_QUALITY;			// DEFAULT_QUALITY;	PROOF_QUALITY;
		lFont.lfPitchAndFamily = ( DEFAULT_PITCH | FF_DONTCARE );		// ( FIXED_PITCH | FF_MODERN );
		assert(strcpy_s(lFont.lfFaceName, sizeof(lFont.lfFaceName), _fontName.c_str()) == 0);
		CV_Assert( ( hFont = CreateFontIndirect( &lFont ))!= NULL );

		hdc = GetDC( NULL );
		oldFont = (HFONT) SelectObject( hdc, hFont );
		GetTextMetrics( hdc, &tm );
}


putcharJP::~putcharJP(void)
{
	SelectObject( hdc, oldFont );
	DeleteObject( hFont );
	ReleaseDC( NULL, hdc );
}


// 1文字描画
void putcharJP::putChar( Mat &mat, char *c, Point2i pos, Scalar sc, int _gray){
	CV_Assert( ( _gray == GGO_GRAY2_BITMAP )||( _gray == GGO_GRAY4_BITMAP )||( _gray == GGO_GRAY8_BITMAP )||( _gray == GGO_BITMAP ));
	CV_Assert( mat.type() == CV_8UC3 );
	//
	int iLevel = 2;
	switch( _gray ){
	case GGO_GRAY2_BITMAP:
		iLevel = 5;
		break;
	case GGO_GRAY4_BITMAP:
		iLevel = 17;
		break;
	case GGO_GRAY8_BITMAP:
		iLevel = 65;
		break;
	}
	GLYPHMETRICS gm;
	memset( &gm, 0, sizeof(gm) );
	const MAT2 mat2 = {{ 0, 1 },{ 0, 0 },{ 0, 0 },{ 0, 1 }};

	UINT code = 0;
	// マルチバイト文字の場合、
	// 1バイト文字のコードは1バイト目のUINT変換、
	// 2バイト文字のコードは[先導コード]*256 + [文字コード]です
	if( IsDBCSLeadByte( *c )){
		code = (((BYTE) c[ 0 ] << 8 ) | ( (BYTE) c[ 1 ] ));
	}
	else {
		code = c[0];
	}
	//#endif
	DWORD rSize = ::GetGlyphOutline(	// 最初のGetGryphOutline関数はcbBufferパラメータに0を渡し、
		hdc,
		code,			// 結果のデータを格納するためのサイズだけを返してもらう
		GGO_GRAY8_BITMAP,	// TT なら、こっち GGO_BEZIER|GGO_NATIVE|GGO_UNHINTED,
		&gm,
		0,
		NULL,
		&mat2 );
	CV_Assert( rSize != GDI_ERROR );	// 失敗した場合 GDI_ERRORが返る（0でないので注意）
	// 正しいバッファサイズが返ってきた
	char *buff = new char[ rSize ];		// 得られたサイズでバッファを生成する
	rSize = GetGlyphOutline(		// バッファのサイズと先頭アドレスを渡し、
		hdc,							// アウトラインのデータをそこに取得する
		code,
		GGO_GRAY8_BITMAP,	// TT なら、こっち GGO_BEZIER|GGO_NATIVE|GGO_UNHINTED,
		&gm,
		rSize,
		buff,
		&mat2 );
	CV_Assert( rSize != GDI_ERROR );	// 失敗した場合 GDI_ERRORが返る（0でないので注意）
	//
	// ここで得られたグリフの処理を行う
	//
	if( rSize > 0 ){
		// iOfs_x, iOfs_y : 書き出し位置(左上)
		// iBmp_w, iBmp_h : フォントビットマップの幅高
		int iOfs_x = gm.gmptGlyphOrigin.x + pos.x;
		int iOfs_y = tm.tmAscent - gm.gmptGlyphOrigin.y + pos.y + iCursor_y;
		int iBmp_w = gm.gmBlackBoxX + (4 - (gm.gmBlackBoxX % 4)) % 4;
		int iBmp_h = gm.gmBlackBoxY;
		if (int line = (iOfs_x + iBmp_w) / mat.cols > 0)
		{
			iCursor_y += 32;
			pos.x = 0;
			iOfs_x = gm.gmptGlyphOrigin.x + pos.x;
			iOfs_y = tm.tmAscent - gm.gmptGlyphOrigin.y + pos.y + iCursor_y;
		}
		if (iOfs_y + iBmp_h > mat.rows) goto END;
		//
#pragma omp parallel for
		for( int y = iOfs_y; y < iOfs_y + iBmp_h; y++ ){
			for( int x = iOfs_x; x < iOfs_x+ (int)gm.gmBlackBoxX; x++ ){
				DWORD Alpha = GRAY_LEVEL( buff[ x - iOfs_x + iBmp_w *( y - iOfs_y )]);
				UCHAR *p = mat.ptr( y, x );
				if( Alpha != 0 ){
					if( Alpha == 255 ){
						*( p )     =(UCHAR) sc[ 0 ];
						*( p + 1 ) =(UCHAR) sc[ 1 ];
						*( p + 2 ) =(UCHAR) sc[ 2 ];
					}
					else {;
					// 混ぜ
					double alp1 = (double) Alpha / 255.0;		// 書く色の割合
					double alp2 = 1.0 - alp1;					// 元の色の割合
					double B0 = *( p )    ;	// 元の色
					double G0 = *( p + 1 );
					double R0 = *( p + 2 );
					double B = B0 * alp2 + sc[ 0 ] * alp1;
					double G = G0 * alp2 + sc[ 1 ] * alp1;
					double R = R0 * alp2 + sc[ 2 ] * alp1;
					*( p )     =(UCHAR) B;
					*( p + 1 ) =(UCHAR) G;
					*( p + 2 ) =(UCHAR) R;
					}
				}
			}
		}
	}
	iCursor_x = pos.x + gm.gmCellIncX;
	//
END:
	delete[] buff; // バッファ解放
}


// 文字列描画
void putcharJP::putText( Mat &mat, TCHAR *c, Point2i pos, Scalar sc, int _gray){
	CV_Assert( ( _gray == GGO_GRAY2_BITMAP )||( _gray == GGO_GRAY4_BITMAP )||( _gray == GGO_GRAY8_BITMAP )||( _gray == GGO_BITMAP ));
	CV_Assert( mat.type() == CV_8UC3 );
	//
	while( *c ){
		putChar( mat, c, pos, sc, _gray );
		c+=( IsDBCSLeadByte( *c ) ? 2 : 1 );
		pos.x = iCursor_x;
	}
}



void putcharJP::putFittingChar(cv::Mat *mat, std::string c, int margin, cv::Scalar sc, cv::Scalar bg_color, int _gray)
{
	char chr[3];
	chr[0] = c.c_str()[0];
	if(IsDBCSLeadByte(*c.c_str()))
	{
		chr[1] = c.c_str()[1];
		chr[2] = '\0';
	}
	else 
		chr[1] = '\0';

	putFittingChar(mat, chr, margin, sc, bg_color, _gray);
}

// 1文字描画
void putcharJP::putFittingChar( Mat *mat, char *c, int margin, Scalar sc, Scalar bg_color, int _gray){
	CV_Assert( ( _gray == GGO_GRAY2_BITMAP )||( _gray == GGO_GRAY4_BITMAP )||( _gray == GGO_GRAY8_BITMAP )||( _gray == GGO_BITMAP ));
	//
	int iLevel = 2;
	switch( _gray ){
	case GGO_GRAY2_BITMAP:
		iLevel = 5;
		break;
	case GGO_GRAY4_BITMAP:
		iLevel = 17;
		break;
	case GGO_GRAY8_BITMAP:
		iLevel = 65;
		break;
	}
	GLYPHMETRICS gm;
	memset( &gm, 0, sizeof(gm) );
	const MAT2 mat2 = {{ 0, 1 },{ 0, 0 },{ 0, 0 },{ 0, 1 }};

	UINT code = 0;
	// マルチバイト文字の場合、
	// 1バイト文字のコードは1バイト目のUINT変換、
	// 2バイト文字のコードは[先導コード]*256 + [文字コード]です
	if( IsDBCSLeadByte( *c )){
		code = (((BYTE) c[ 0 ] << 8 ) | ( (BYTE) c[ 1 ] ));
	}
	else {
		code = c[0];
	}
	DWORD rSize = ::GetGlyphOutline(	// 最初のGetGryphOutline関数はcbBufferパラメータに0を渡し、
		hdc,
		code,			// 結果のデータを格納するためのサイズだけを返してもらう
		GGO_GRAY8_BITMAP,	// TT なら、こっち GGO_BEZIER|GGO_NATIVE|GGO_UNHINTED,
		&gm,
		0,
		NULL,
		&mat2 );
	CV_Assert( rSize != GDI_ERROR );	// 失敗した場合 GDI_ERRORが返る（0でないので注意）
	// 正しいバッファサイズが返ってきた
	char *buff = new char[ rSize ];		// 得られたサイズでバッファを生成する
	rSize = GetGlyphOutline(		// バッファのサイズと先頭アドレスを渡し、
		hdc,							// アウトラインのデータをそこに取得する
		code,
		GGO_GRAY8_BITMAP,	// TT なら、こっち GGO_BEZIER|GGO_NATIVE|GGO_UNHINTED,
		&gm,
		rSize,
		buff,
		&mat2 );
	CV_Assert( rSize != GDI_ERROR );	// 失敗した場合 GDI_ERRORが返る（0でないので注意）
	//
	// ここで得られたグリフの処理を行う
	//
	if( rSize > 0 ){
		// iOfs_x, iOfs_y : 書き出し位置(左上)
		// iBmp_w, iBmp_h : フォントビットマップの幅高
		int iOfs_x = gm.gmptGlyphOrigin.x;
		int iOfs_y = tm.tmAscent - gm.gmptGlyphOrigin.y;
		int iBmp_w = gm.gmBlackBoxX + ( 4 -( gm.gmBlackBoxX % 4 )) % 4;
		int iBmp_h = gm.gmBlackBoxY;
		//

		*mat = Mat(Size(gm.gmBlackBoxX + margin*2, gm.gmBlackBoxY + margin*2), CV_8UC3, bg_color);


#pragma omp parallel for
		for( int y = 0; y < (int)gm.gmBlackBoxY; y++ ){
			for (int x = 0; x < (int)gm.gmBlackBoxX; x++){
				DWORD Alpha = GRAY_LEVEL( buff[x + iBmp_w*y]);
				UCHAR *p = mat->ptr( y+margin, x+margin );
				if( Alpha != 0 ){
					if( Alpha == 255 ){
						*( p )     =(UCHAR) sc[ 0 ];
						*( p + 1 ) =(UCHAR) sc[ 1 ];
						*( p + 2 ) =(UCHAR) sc[ 2 ];
					}
					else {;
					// 混ぜ
					double alp1 = (double) Alpha / 255.0;		// 書く色の割合
					double alp2 = 1.0 - alp1;					// 元の色の割合
					double B0 = *( p )    ;	// 元の色
					double G0 = *( p + 1 );
					double R0 = *( p + 2 );
					double B = B0 * alp2 + sc[ 0 ] * alp1;
					double G = G0 * alp2 + sc[ 1 ] * alp1;
					double R = R0 * alp2 + sc[ 2 ] * alp1;
					*( p )     =(UCHAR) B;
					*( p + 1 ) =(UCHAR) G;
					*( p + 2 ) =(UCHAR) R;
					}
				}
			}
		}
	}
	//iCursor_x = pos.x + gm.gmCellIncX;
	//
	delete[] buff; // バッファ解放
}

void putcharJP::putFittingChar( Mat *mat, string c, int width, int height, Scalar sc, Scalar bg_color, int _gray){
		Mat src;
		putFittingChar(&src, c, 0, sc, bg_color);

		// 幅と高さを決定
		if(src.cols > width)
		{
			cout << c << " の幅が指定のサイズより大きいため，サイズ指定を無視します．" << endl;
			width = src.cols;
		}
		if(src.rows > height)
		{
			cout << c << " の高さが指定のサイズより大きいため，サイズ指定を無視します．" << endl;
			height = src.rows;
		}

		*mat = Mat(Size(width, height), CV_8UC3, bg_color);
		Rect rect((width-src.cols)/2, (height-src.rows)/2, src.cols, src.rows);
		Mat roi(*mat, rect);
		src.copyTo(roi);
}